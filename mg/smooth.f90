subroutine smooth(n,v,f,omega,niter)

implicit none

integer,                      intent(in)    :: n     !Matrix dimension
integer,                      intent(in)    :: niter !Iterations to perform
real(kind=8),                 intent(in)    :: omega !weighting for method
real(kind=8), dimension(n-1), intent(in)    :: f     !RHS
real(kind=8), dimension(n-1), intent(inout) :: v     !Initial guess
real(kind=8), dimension(n-1)                :: vtemp !Intermediary v
integer                                     :: iter  !Iteration counter
integer                                     :: i     !counter


!Weighted Jacobi Iteration

do iter = 1,niter
         
!   print*, 'smooth'
   vtemp(1) = 0.5_8*(v(2)+f(1)/n**2)
   do i=2,n-2
      vtemp(i)=0.5_8*(v(i-1)+v(i+1)+f(i)/n**2)
   end do
   vtemp(n-1) = 0.5_8*(v(n-2)+f(n-1)/n**2)

   do i=1,n-1
      v(i)=(1-omega)*v(i)+omega*vtemp(i)
   end do

end do

end subroutine smooth

subroutine gs_iterate(n,v,f,tol,niter,nitermax,conv,printtable)

  implicit none

  integer, intent(in) :: n, nitermax, printtable !Matrix dimension, maximum iteration limit, Boolean print
  real(kind=8), intent(in) :: tol !Tolerance
  real(kind=8), intent(in), dimension(n-1) :: f !RHS
  real(kind=8), intent(inout), dimension(n-1) :: v !Initial guess
  real(kind=8), dimension(n-1) :: r !Residual
  real(kind=8) :: res, res0, resold !Residual values
  real(kind=8), intent(out) :: conv !Convergence
  integer, intent(out) :: niter !Iteration counter
  integer :: i !counter


  !Set up variables ready to begin algorithm
  call residual(n,v,f,r)
  call norm2(n-1,r,res0)
  resold = res0

  if (printtable==1) then
     print*, "  Iteration ", " Residual                ", "  residual/residual_0     ", "  Convergence"
     print*, "  - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - "
  end if
  do niter = 0,nitermax

     !Check to see whether tolerance is met
     call residual(n,v,f,r)
     call norm2(n-1,r,res)
     conv = res/resold
     if (printtable==1) then
        print*, niter, res, res/res0, conv
     end if
     if (res/res0 < tol) then
        exit
     end if

     !If not then do a step of the Gauss-Seidel Iteration
     resold = res

     v(1) = 0.5_8*(v(2)+f(1)/n**2)
     do i=2,n-2
        v(i)=0.5_8*(v(i-1)+v(i+1)+f(i)/n**2)
     end do
     v(n-1) = 0.5_8*(v(n-2)+f(n-1)/n**2)

  end do

end subroutine gs_iterate

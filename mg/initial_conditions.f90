subroutine initial_conditions(n,u,k)

  implicit none
  integer,                    intent(in)  :: n  !Matrix dimension
  real(kind=8), dimension(n), intent(out) :: u  !Actual solution
  integer,                    intent(in)  :: k  !Number of modes
  integer                                 :: i  !Counter
  real(kind=8)                            :: pi !
  pi = 4.0_8*atan(1.0_8)

  
  do i=1,n-1
     u(i)=sin(k*i*pi/n)
  end do

  

end subroutine initial_conditions

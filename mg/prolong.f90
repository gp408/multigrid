subroutine prolong(oldn,oldv,newv)

  implicit none
  integer,                           intent(in)  :: oldn !Original dimension
  real(kind=8), dimension(oldn-1),   intent(in)  :: oldv !Smaller vector to be prologed
  real(kind=8), dimension(2*oldn-1), intent(out) :: newv !Larger vector to output
  integer                                        :: i    !counter

  newv(1) = 0.5_8*oldv(1)
  do i=1,oldn-2
     newv(2*i) = oldv(i)
     newv(2*i+1) = 0.5_8*(oldv(i) + oldv(i+1))
  end do
  newv(2*oldn-2) = oldv(oldn-1)
  newv(2*oldn-1) = 0.5_8*oldv(oldn-1)

!  print*,
!  print*, 'Prolonging'
!  print*, 
!  print*, 'Old v ', oldv
!  print*,
!  print*, 'New v', newv
!  print*,

  
end subroutine prolong

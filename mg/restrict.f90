subroutine restrict(newn,oldv,newv)

  implicit none
  integer,                           intent(in)  :: newn !Smaller dimension
  real(kind=8), dimension(2*newn-1), intent(in)  :: oldv !Larger vector to be restricted
  real(kind=8), dimension(newn-1),   intent(out) :: newv !Smaller vector to output
  integer                                        :: i    !Counter

  do i = 1,newn-1
     newv(i) = oldv(2*i)
     !newv(i) = 0.25_8*( oldv(2*i-1) + 2.0_8*oldv(2*i) + oldv(2*i+1) )
  end do

!  print*,
!  print*, 'Restricting'
!  print*,
!  print*, 'Old v', oldv
!  print*,
!  print*, 'New v', newv
!  print*,
  
end subroutine restrict

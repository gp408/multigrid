subroutine residual(n,v,f,r)

  implicit none
  integer,                      intent(in)  :: n !Matrix dimension
  real(kind=8), dimension(n-1), intent(in)  :: v !Iterate solution
  real(kind=8), dimension(n-1), intent(in)  :: f !RHS
  real(kind=8), dimension(n-1), intent(out) :: r !Residual
  integer                                   :: i !Counter
  
  r(1)=1./n**2*f(1)-2.0_8*v(1)+v(2)
  do i=2,n-2
     r(i)=1./n**2*f(i)+v(i-1)-2.0_8*v(i)+v(i+1)
  end do
  r(n-1)=1./n**2*f(n-1)+v(n-2)-2.0_8*v(n-1)

  r = (n**2)*r

end subroutine residual

program main
  
  implicit none
  integer                                   :: n          !Matrix dimension
  real(kind=8), dimension(:,:), allocatable :: v          !Iterate MG array
  real(kind=8), dimension(:,:), allocatable :: r          !Residual MG array
  real(kind=8), dimension(:),   allocatable :: f          !RHS
  real(kind=8), dimension(:),   allocatable :: u          !Exact solution
  integer                                   :: L          !Number of Multigrid levels
  integer                                   :: k          !Number of modes
  real(kind=8)                              :: h          !Spacing
  real(kind=8), dimension(:),   allocatable :: rtemp      !Temporary storage for residual
  real(kind=8)                              :: res0       !Initial residual norm
  real(kind=8)                              :: res        !Current residual norm
  integer                                   :: cyclmax    !Cycle iterations
  integer                                   :: smoomax    !Smoother iterations
  integer                                   :: itermax    !Solver iterations
  real(kind=8)                              :: tol        !Tolerance
  real(kind=8)                              :: omega      !Jacobi weightng
  real(kind=8)                              :: acc        !Accuracy
  integer                                   :: printtable !Boolean for printing
  integer                                   :: i          !Counter
  
  
  print*, 'Matrix Dimension n? '
  read*, n
  !print*, 'Number of modes? '
  !read*, k
  k = 1
  print*, 'No. MG Levels? '
  read*, L
  
  
  if ((mod(n,2**(L-1)) .NE. 0) .OR. (n .EQ. 2**(L-1))) then
     n = 2**(L-1)*(1+max(n/(2**(L-1)),1))
     print*, 
     print*, 'n was adjusted to fit with Multigrid restriction'
  end if

  cyclmax = 10000
  smoomax = 2
  itermax = 256
  tol = 1.E-8
  h = 1.0_8/n
  printtable = 0
  omega = 0.666_8
  
  
  print*,
  print*, '**************************PARAMETERS**************************'
  print*, '=============================================================='
  print*, 'Matrix Dimension,          n =', n
  print*, 'Spacing,                   h =', h
  print*, 'No. modes,                 k =', k
  print*, 'Tolerance,               tol =', tol
  print*, 'Jacobi weighting,      omega =', omega
  print*, 'MG Levels,                 L =', L
  print*, 'Dimension at coarsest level  =', n/2**(L-1)
  print*, 'Maximum cycles,      cyclmax =', cyclmax
  print*, 'Smoother iterations, smoomax =', smoomax
  print*, 'Solver iterations,   itermax =', itermax
  print*, '=============================================================='
  print*,
  print*,
  
  
  !Alloocate storage
  allocate(f(n-1))
  allocate(v(L,n-1))
  allocate(r(L,n-1))
  allocate(u(n-1))
  allocate(rtemp(n-1))
  
  !Initilise u
  call initial_conditions(n,u,k)
  
  !  print*, 'Exact Solution = ', u
  
  !Find RHS
  f(1) = n**2*(2.0_8*u(1)-u(2))
  do i = 2,n-2
     f(i) = n**2*(-u(i-1)+2.0_8*u(i)-u(i+1))
  end do
  f(n-1) = n**2*(-u(n-2)+2.0_8*u(n-1))
  
  !  print*, 'RHS = ', f
  
  !Execute Vcycle until accurate enough
  
  v(:,:)=0.0_8
  r(L,:) = f(:)
  
  call residual(n,v(L,:),r(L,:),rtemp)
  call norm2(n-1,rtemp,res0)

  print*, '**************************ITERATIONS**************************'
  print*, '=============================================================='
  print*, '          iter  res/res0'
  
  do i=1,cyclmax
     call Vcycle(n,L,v,r,smoomax,itermax,omega)
     call residual(n,v(L,:),r(L,:),rtemp)
     call norm2(n-1,rtemp,res)

     print*, i, ' ',res/res0
     
     if (res/res0<tol) then
        exit
     end if
  end do

  print*, '=============================================================='
  
  
  call test_accuracy(n,v(L,:),u,acc)
  
  print*, 'Accuracy = ', acc
  print*,
  print*, 
  
  deallocate(f)
  deallocate(v)
  deallocate(r)
  deallocate(u)
  deallocate(rtemp)
  
end program main

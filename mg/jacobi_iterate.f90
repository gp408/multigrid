subroutine jacobi_iterate(n,v,f,omega,tol,nitermax)

  implicit none
  integer,                      intent(in)    :: n        !Dimension
  real(kind=8), dimension(n-1), intent(inout) :: v        !Initial guess and output
  real(kind=8), dimension(n-1), intent(in)    :: f        !RHS
  real(kind=8),                 intent(in)    :: omega    !Jacobi weighting
  real(kind=8),                 intent(in)    :: tol      !Tolerance
  integer,                      intent(in)    :: nitermax !Maximum iteration limit
  real(kind=8), dimension(n-1)                :: r        !Residual
  real(kind=8), dimension(n-1)                :: vold     !Old v
  real(kind=8), dimension(n-1)                :: vtemp    !Intermediary v
  real(kind=8)                                :: res      !Residual value
  real(kind=8)                                :: res0     !Original residual value
  real(kind=8)                                :: resold   !Old residual value
  integer                                     :: niter    !Iteration counter
  integer                                     :: i        !Counter


  !Set up variables ready to begin algorithm
  call residual(n,v,f,r)
  call norm2(n-1,r,res0)
  resold = res0
  vold   = v
  
  
  do niter = 0,nitermax
     
     !Check to see whether tolerance is met
     call residual(n,v,f,r)
     call norm2(n-1,r,res)
     if (res/res0 < tol) then
        exit
     end if
     
     !If not then do a step of the Jacobi Iteration
     vold   = v
     resold = res
     
     vtemp(1) = 0.5_8*(vold(2)+f(1)/n**2)
     do i=2,n-2
        vtemp(i)=0.5_8*(vold(i-1)+vold(i+1)+f(i)/n**2)
     end do
     vtemp(n-1) = 0.5_8*(vold(n-2)+f(n-1)/n**2)
     
     do i=1,n-1
        v(i)=(1-omega)*vold(i)+omega*vtemp(i)
     end do
     
  end do
  
end subroutine jacobi_iterate

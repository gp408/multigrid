recursive subroutine Vcycle(n,L,v,r,smoomax,itermax,omega)

  implicit none
  integer,                        intent(in)    :: n        !Dimension
  integer,                        intent(in)    :: L        !MG Level
  real(kind=8), dimension(L,n-1), intent(inout) :: v        !Approx soln
  real(kind=8), dimension(L,n-1), intent(inout) :: r        !Residual
  integer,                        intent(in)    :: smoomax  !Smoother iterations
  integer,                        intent(in)    :: itermax  !Solver iterations
  real(kind=8),                   intent(in)    :: omega    !Jacobi weight
  real(kind=8), dimension(n-1)                  :: rt       !Temporary

!  print*,
!  print*, 'Beginning level ', L
!  print*,
  
  if (L>1) then

     call smooth(n,v(L,:),r(L,:),omega,smoomax)

!     print*, 'v after smoother at level', L, 'is: ', v(L,:)
!     print*,
     
     call residual(n,v(L,:),r(L,:),rt)

!     print*, 'residual at level', L, 'is: ', rt
!     print*,
     
     call restrict(n/2,rt,r(L-1,1:n/2-1))

!     print*, 'restricted residual is: ', r(L-1,1:n/2-1)
!     print*,
     
     v(L-1,:) = 0.0_8

     call Vcycle(n/2,L-1,v(1:L-1,1:n/2-1),r(1:L-1,1:n/2-1),smoomax,itermax,omega)

!     print*, 'result from recursion: ', v(L-1,1:n/2-1)
!     print*,
     
     call prolong(n/2,v(L-1,:),rt)

!     print*, 'Prolongation: ', rt
!     print*,

     v(L,:) = v(L,:) + rt(:)
     
!     print*, 'v = ', v(L,:)
!     print*, 
     
     
     call smooth(n,v(L,:),r(L,:),omega,smoomax)

!     print*, 'v after postsmoother: ', v(L,:)
!     print*,

!     call residual(n,v(L,:),r(L,:),rt)
!     print*, 'Residual after postsmoother: ', rt
!     print*,
     
  else
     
     !call jacobi_iterate(n,v(1,:),r(1,:),omega,tol,itermax)
     call smooth(n,v(L,:),r(L,:),omega,itermax)
     
!     print*, 'Jacobi gives: ', v(L,:)
!     print*,
     
  end if

!  print*,
!  print*, 'Ending Level ',L
!  print*,
  
end subroutine Vcycle


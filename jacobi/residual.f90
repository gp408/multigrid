subroutine residual(n,v,f,r)
!Takes a n-dimensional system with v as the current approximation and f as the RHS and outputs the residual r
  implicit none
  integer, intent(in) :: n !Matrix dimension
  real(kind=8), intent(in), dimension(n-1) :: v,f !Iterate solution and RHS 
  real(kind=8), intent(out), dimension(n-1) :: r !Residual
  integer :: i !Counter
  
  r(1)=1./n**2*f(1)-2.0_8*v(1)+v(2)
  do i=2,n-2
     r(i)=1./n**2*f(i)+v(i-1)-2.0_8*v(i)+v(i+1)
  end do
  r(n-1)=1./n**2*f(n-1)+v(n-2)-2.0_8*v(n-1)
     

end subroutine residual

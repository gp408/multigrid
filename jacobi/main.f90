program main

  implicit none
  integer :: n, niter, nitermax, i !Matrix dimension, iterations used, maximum iteration limit, counter
  real (kind=8), dimension(:), allocatable :: f,v,u !RHS, iterate, exact
  real (kind=8) :: tol, h, k, acc !Tolerance, discretisation width, number of modes in solution, Accuracy
  real (kind=8) :: omega !Weightinf for the jacobi method
  real (kind=8) :: t_start, t_finish, th_conv, ac_conv !Timing variables, convergences (theoretical and actual)
  integer :: printtable !Boolean for printing tables of iterations
  real(kind=8) :: pi
  pi = 4.0_8*atan(1.0_8)
  
  print*, 'Matrix Dimension n? '
  read*, n
  print*, 'Number of modes? '
  read*, k

  nitermax = 256
  tol = 0.01
  h = 1.0_8/n
  printtable = 1
  omega = 1.0_8

  print*, 
  print*, '**************************PARAMETERS**************************'
  print*, '=============================================================='
  print*, 'Matrix Dimension,         n =', n
  print*, 'Spacing,                  h =', h
  print*, 'No. modes,                k =', k
  print*, 'Iteration limit,   nitermax =', nitermax
  print*, 'Tolerance,              tol =', tol
  print*, 'Jacobi weighting,     omega =', omega
  print*, '=============================================================='
  print*,
  print*,
  
  !Alloocate storage
  allocate(f(n-1))
  allocate(v(n-1))
  allocate(u(n-1))
  
  !Initilise u
  call initial_conditions(n,u,k)

  !Find RHS
  f(1) = n**2*(2.0_8*u(1)-u(2))
  do i = 2,n-2
     f(i) = n**2*(-u(i-1)+2.0_8*u(i)-u(i+1))
  end do
  f(n-1) = n**2*(-u(n-2)+2.0_8*u(n-1))

  !Run Jacobi iteration
  print*, '*************************JACOBI METHOD************************'
  v(:) = 0.0_8
  call cpu_time(t_start)
  call jacobi_iterate(n,v,f,omega,tol,niter,nitermax,ac_conv,printtable)
  call cpu_time(t_finish)
  
  !Check accuracy
  call test_accuracy(n,v,u,acc)
  th_conv = 1.0_8-2.0_8*omega*(sin(k*pi/(2.0_8*n)))**2
  call printer(acc,niter,nitermax,t_start,t_finish,th_conv,ac_conv)

  !Run Gauss-Seidel iteration
  print*, '**********************GAUSS-SEIDEL METHOD*********************'
  v(:) = 0.0_8
  call cpu_time(t_start)
  call gs_iterate(n,v,f,tol,niter,nitermax,ac_conv,printtable)
  call cpu_time(t_finish)

  !Check accuracy
  call test_accuracy(n,v,u,acc)
  th_conv = (cos(k*pi/n))**2
  call printer(acc,niter,nitermax,t_start,t_finish,th_conv,ac_conv)


  deallocate(f)
  deallocate(v)
  deallocate(u)

end program main

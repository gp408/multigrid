subroutine jacobi_iterate(n,v,f,omega,tol,niter,nitermax,conv,printtable)

implicit none

integer, intent(in) :: n, nitermax, printtable !Matrix dimension, maximum iteration limit, Boolean print
real(kind=8), intent(in) :: omega, tol !weighting for method, Tolerance
real(kind=8), intent(in), dimension(n-1) :: f !RHS
real(kind=8), intent(inout), dimension(n-1) :: v !Initial guess
real(kind=8), dimension(n-1) :: r, vold, vtemp !Residual, old v, intermidiary v
real(kind=8) :: res, res0, resold !Residual values
integer, intent(out) :: niter !Iteration counter
real(kind=8), intent(out) :: conv !Convergence
integer :: i !counter


!Set up variables ready to begin algorithm
call residual(n,v,f,r)
call norm2(n-1,r,res0)
resold = res0
vold   = v

if (printtable==1) then
   print*, "  Iteration ", " Residual                ", "  residual/residual_0     ", "  Convergence"
   print*, "  - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - "
end if
   
do niter = 0,nitermax
   
   !Check to see whether tolerance is met
   call residual(n,v,f,r)
   call norm2(n-1,r,res)
   conv = res/resold
   if (printtable==1) then
      print*, niter, res, res/res0, conv
   end if
   if (res/res0 < tol) then
      exit
   end if
      
   !If not then do a step of the Jacobi Iteration
   vold   = v
   resold = res
   
   vtemp(1) = 0.5_8*(vold(2)+f(1)/n**2)
   do i=2,n-2
      vtemp(i)=0.5_8*(vold(i-1)+vold(i+1)+f(i)/n**2)
   end do
   vtemp(n-1) = 0.5_8*(vold(n-2)+f(n-1)/n**2)

   do i=1,n-1
      v(i)=(1-omega)*vold(i)+omega*vtemp(i)
   end do

end do

end subroutine jacobi_iterate

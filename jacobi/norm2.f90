subroutine norm2(n,v,nrm)

  implicit none
  integer, intent(in) :: n !Matrix dimension
  real(kind=8), intent(in), dimension(n) :: v !Vecotor to find norm of
  real(kind=8), intent(out) :: nrm !Norm of v
  integer :: i !Counter

  nrm = 0
  do i = 1,n
     nrm = nrm + v(i)**2
  end do
  nrm = sqrt(nrm)
  

end subroutine norm2

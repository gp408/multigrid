subroutine printer(acc,niter,nitermax,t_start,t_finish,th_conv,ac_conv)

  implicit none
  !A collection of variables to be printed
  real(kind=8), intent(in) ::  acc, t_start, t_finish, th_conv, ac_conv
  integer, intent(in) :: niter, nitermax
  
  print*,
  print*,
  print*, '****************************RESULTS***************************'
  print*, '=============================================================='
  print*, 'Error with actual soln, acc =', acc
  print*, 'Number of iterations, niter =', niter
  if (niter>=nitermax) then
     print*, 'MAXIMUM ITERATIONS REACHED:  ', nitermax
  end if
  print*, 'Time elapsed,             t =', t_finish-t_start
  print*, 'Time per iteration, t/niter =', (t_finish-t_start)/niter
  print*,
  print*, 'Theoretical convergece      =', th_conv
  print*, 'Actual convergence          =', ac_conv
  print*, '=============================================================='
  print*,
  print*,
  print*,
  print*,
  
end subroutine printer

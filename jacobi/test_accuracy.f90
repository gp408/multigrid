subroutine test_accuracy(n,v,u,acc)

  implicit none
  integer, intent(in) :: n !Matrix Dimension
  real(kind=8), intent(in), dimension(n-1) :: v,u !Approximated solution, Actual Solution
  real(kind=8), intent(out) :: acc !Accuracy
  real(kind=8) :: unrm !norm of u
  real(kind=8), dimension(n-1) :: temp

  temp = v
  call daxpy(n-1,-1.0_8,u,1,temp,1)
  call norm2(n-1,temp,acc)
  call norm2(n-1,u,unrm)
  acc = acc/unrm
  
end subroutine test_accuracy
